# 前言

在linux下要開始c++專案，最後一定會想要用到makefile來編譯，然後再用sudo make install來安裝binary檔案。   
然後，makefile對一般人來說，還是算有一點複雜，當專案複雜度增加，更難撰寫了。因此，在開源軟體界中有兩套可以協助自動產生makefile的工具：

- Autotool
- Cmake

Autotool是GNU團體開發的工具，所以如果是跟ubuntu官方相關的套件，其源始碼幾乎都是使用autotool環境開發的。

這篇就是要介紹Autotool要怎麼建立一個helloworld的專案。

# 參考資料

- [A tutorial for porting to autoconf & automake](http://mij.oltrelinux.com/devel/autoconf-automake/)
- [The magic behind configure, make, make install](https://robots.thoughtbot.com/the-magic-behind-configure-make-make-install#creating-the-makefile)
- [Creating a project Using Autotools | GNOME](https://developer.gnome.org/anjuta-build-tutorial/stable/create-autotools.html.en)
- [Adding a library Using Autotools | GNOME](https://developer.gnome.org/anjuta-build-tutorial/stable/library-autotools.html.en)

# 正文

### 建立好目錄。

如：

```
﹂ project
     ﹂ src
         ﹂ helloworld.c
```

### 使用指令`autoscan`

使用此指令來自動建立使用autotool會需要用到的檔案。   
執行完後，你會出現多出了以下檔案：
- project/configure.scan
- project/autoscan.log

### 將configure.scan改名為configure.ac。


### 修改configure.ac內容

將*AC_INIT(FULL-PACKAGE-NAME, VERSION, BUG-REPORT-ADDRESS)*這行的參數改為此project自定義的值。

例如：

```
AC_INIT(helloworld, 1.0, eric.hsu@hwancom.com)
```

然後將AC_CONFIG_HEADERS那行刪掉，因為我們沒有config.h檔案。

然後再將AC_CONFIG_SRCDIR那行刪掉，因為之後會使用AC_CONFIG_FILES來取代。

若是要用C++編譯器，要將AC_PROG_CC改成AC_PROG_CXX

### 使用`autoconf`指令產生configure檔

執行autoconf後，會產生以下檔案：
- project/configure
- project/autom4te.cache

### 新增Makefile.am檔

Makefile.am有點像makefile的template，用來給automake來自動產生makefile用的。

我們需要在兩個地方新增Makefile.am檔：
- 專案根目錄
- src目錄

首先，先新增專案根目錄的Makefile.am，內容打上：

```
AUTOMAKE_OPTIONS = foreign
SUBDIRS = src
```

foreign的意思就是非GNU標準格式。因為GNU的開發團隊必須遵守他們制定的開發標準，但我們不是他們團隊，所以就打上foreign，避免不必要錯誤訊息。

SUBDIRS就是用來新增子目錄用的，目前我們只有src子目錄，如果還有其它的，例如example、scripts、doc、man等，就空一格加上子目錄名稱就可以了。


再來，新增子目錄的Makefile.am，內容打上：

```
AM_CFLAGS = -std=c11 -Wall
AM_LDFLAGS = 

bin_PROGRAMS = helloworld
helloworld_SOURCES = helloworld.c
```

若是使用C++編譯器的話，AM_CFLAGS要改成AM_CXXFLAGS


### 再編輯一次configure.ac

增加
- AM_INIT_AUTOMAKE：放在AC_INIT之後
- AC_CONFIG_FILES：放在AC_OUTPUT之前

最終的configure.ac會長這樣：

```
AC_PREREQ([2.69])
AC_INIT(helloworld, 1.0, eric.hsu@hwacom.com)
AM_INIT_AUTOMAKE

# Checks for programs.
AC_PROG_CC

AC_CONFIG_FILES(
Makefile
src/Makefile
)

AC_OUTPUT
```

### 執行`aclocal`

執行aclocal會建立aclocal.m4，裡面有我們要做用autotool會用到的macros。例如：AM_INIT_AUTOMAKE。

### 執行`automake`

打上指令：

```
$ automake --add-missing
```

執行完後，你會發現多了很多檔案，那些都不管，主要是會有Makefile.in這檔案。


### 執行`autoconf`

更新configure執行檔

### 執行`./configure`

此時，會產生Makefile檔

### 執行`make dist`
若要發佈專案，其實終端用戶是可以不用知道你到底是用什麼工具產生configure、Makefile檔案的，
所以autotool提供指令`make dist`來包裝distribution版本。打上指令後，你會發現多出一個檔案叫
*helloworld-1.0.tar.gz*，將之解壓縮後，你會發現裡面很乾淨，拿到此dist版本的人，可以直接解壓縮，
然後執行`./configure && make && sudo make install`，就可以很簡單地安裝你開發的軟體了。

### 執行`make`編譯程式

此時，會在src目錄下看到binary程式

### 執行`sudo make install`來安裝binary程式


# 在eclipse上建立autotools專案


### 創建專案

- 打開eclipse
- 點選File -> New -> C/C++ Project
- 點選C++ Managed Build
- 按Next
- 輸入Project Name
- 在下方Project type中，選擇GNU Autotools | Hello World C++ Project
- 按Next
- 輸入Author、Copyright notice
- 按Next
- 按Finish

### 建置專案並執行

- 點選Project -> Build Project
- 點選左側Project Explorer -> <your_project> -> Binaries -> a.out，對其點右鍵，Run As -> Local C/C++ Application

### 加入External Library

我們將helloworld.cpp的內容改成，來模擬加入xml2這個External Library：

```cpp
#include <libxml/parser.h>			
#include <stdio.h>
			
int main()
{				
	xmlDocPtr doc;                                                                                                                  
	doc = xmlParseFile ("testfile.xml");

	if (doc == NULL) {
		printf ("Document not parsed successfully. \n");
		return -1;
	}
	else {
		printf ("Document parsed successfully.\n");
		xmlFreeDoc(doc);
		return 0;
	}
}
```


這隻程式如果用command line編譯的話，是…

```bash
$ g++ -I/usr/include/libxml2 -lxml2 main.cpp
```

所以如果在eclipse上沒有設定include directories和link libraries的話，是會出錯的。

那要如何設定呢？

因為我摸索也爬文了好久，在IDE上找不到一個方法是可以按一下按扭就可以解決這件事的。

因此，我們會需要設定以下兩個檔：

- configure.ac
- src/Makefile.am


首先，在configure.ac中加入`PKG_CHECK_MODULES(XML, libxml-2.0 >= 2.4)`，加在AC_PROG_CXX後面。
PKG_CHECK_MODULES會利用第二個參數去找/usr/lib/pkgconfig或是/usr/local/lib/pkgconfig裡面的.pc檔。以這個例子來說，PKG_CHECK_MODULES就會去這兩個路徑找libxml-2.0.pc這個檔，然後將資料傳給XML這個變數。

什麼叫將資料傳給XML這個變數？
最重要的就是Libs跟Cflags這兩個值，會將這兩個值傳給XML_LIBS和XML_CFLAGS。

修改後完整的configure.ac內容：

```
AC_PREREQ(2.59)
AC_INIT(helloworld, 1.0)
AC_CANONICAL_SYSTEM
AM_INIT_AUTOMAKE()
AC_PROG_CXX
PKG_CHECK_MODULES(XML, libxml-2.0 >= 2.4)
AC_CONFIG_FILES(Makefile src/Makefile)
AC_OUTPUT
```


再來，就是要在src/Makefile.am中加入`a_out_CPPFLAGS`和`a_out_LIBS`。

修改後完整的src/Makefile.am內容：

```
bin_PROGRAMS=a.out
a_out_SOURCES=helloworld.cpp
a_out_CPPFLAGS = $(XML_CFLAGS)
a_out_LDFLAGS= $(XML_LIBS)
```

設定好後，就可以重新建置，並且執行Binary看看！


